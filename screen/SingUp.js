import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { useState } from 'react';
import { StyleSheet, Text, View, Pressable, TouchableHighlight, Button as RNButton } from 'react-native';

import { Button, InputField, ErrorMessage, Navbar } from '../components';


export default function SignupScreen() {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [number, setPhone] = useState('');
  const [day, setBirthDay] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisibility, setPasswordVisibility] = useState(true);
  const [rightIcon, setRightIcon] = useState('eye');


  const handlePasswordVisibility = () => {
    if (rightIcon === 'eye') {
      setRightIcon('eye-off');
      setPasswordVisibility(!passwordVisibility);
    } else if (rightIcon === 'eye-off') {
      setRightIcon('eye');
      setPasswordVisibility(!passwordVisibility);
    }
  };

  const singUp = async () =>{
    console.log(email, password)
    const res = await fetch('http://localhost:3000/users/signup', {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                  full_name: name,
                  email: email,
                  phone: number,
                  birth_date: day,
                  password: password
                })                
            })
            console.log(await res.json())
  }

  return (
    <View style={styles.container}>
      <StatusBar style='dark-content' />
      <Text style={styles.title}>Crear cuenta nueva.</Text>
      <InputField
        inputStyle={{
          fontSize: 14
        }}
        containerStyle={{
          backgroundColor: '#F6F6F6',
          marginBottom: 20
        }}
        placeholder='Email'
        autoCapitalize='none'
        keyboardType='email-address'
        textContentType='emailAddress'
        autoFocus={true}
        value={email}
        onChangeText={text => setEmail(text)}
      />
      <InputField
        inputStyle={{
          fontSize: 14
        }}
        containerStyle={{
          backgroundColor: '#F6F6F6',
          marginBottom: 20
        }}
        placeholder='Name'
        autoCapitalize='none'
        keyboardType='Name-Person'
        textContentType='NamePerson'
        autoFocus={true}
        value={name}
        onChangeText={text => setName(text)}
      />
      <InputField
        inputStyle={{
          fontSize: 14
        }}
        containerStyle={{
          backgroundColor: '#F6F6F6',
          marginBottom: 20
        }}
        placeholder='Phone Number'
        autoCapitalize='none'
        keyboardType='phone-pad'
        textContentType='PhoneNumber'
        autoFocus={true}
        value={number}
        onChangeText={number => setPhone(number)}
      />
      <InputField
        inputStyle={{
          fontSize: 14
        }}
        containerStyle={{
          backgroundColor: '#F6F6F6',
          marginBottom: 20
        }}
        placeholder='Birth date'
        autoCapitalize='none'
        keyboardType='numbers-and-punctuation'
        textContentType='BirthDate'
        autoFocus={true}
        value={day}
        onChangeText={text => setBirthDay(text)}
      />
      <InputField
        inputStyle={{
          fontSize: 14
        }}
        containerStyle={{
          backgroundColor: '#F6F6F6',
          marginBottom: 20
        }}
        placeholder='Password'
        autoCapitalize='none'
        autoCorrect={false}
        secureTextEntry={passwordVisibility}
        textContentType='password'
        rightIcon={rightIcon}
        value={password}
        onChangeText={text => setPassword(text)}
        handlePasswordVisibility={handlePasswordVisibility}
      />
      <TouchableHighlight style={[styles.btn]} onPress={singUp}>        
          <Text style={[styles.btnText] }>Registrarse</Text>
      </TouchableHighlight>
      <Pressable>
        <Text style={[styles.lastPressable] }>Already have an account? Login!</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingTop: 50,
    paddingHorizontal: 12
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#000000',
    alignSelf: 'center',
    paddingBottom: 24
  },
  btn: {
    overflow: "hidden",
    borderRadius: 100,
    backgroundColor: "#EB5757",
    height: 51,
  },
  lastPressable: {
    paddingTop: 16,
    fontSize: 16,
    textAlign: "center",
    color: "#EB5757",
    fontWeight: "bold",
  },
  btnText: {
    color: "white",
    textAlign: "center",
    fontSize: 16,
    paddingTop: 16,
  }
});

