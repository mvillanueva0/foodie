import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const Profile = () => {
    return (
        <View>
        <View style={styles.profileContainer}>      
            <View style={styles.redBlock}>
            <Text style={styles.logOut}>
                Logout
            </Text>
            <Text style={styles.profile}>
                Profile
            </Text>
            <Image
                style= {styles.imageUser}
                source= {require('../assets/png/User.png')}
            />
            </View>
        </View>
        <View> 
            <View style={styles.whiteBlockName}>
            <Text style={styles.userName}>
                My name My name
            </Text>
            </View>         
            <View style={styles.whiteBlockData}>
            <Image
                style= {styles.icon}
                source= {require('../assets/png/User_Icon.png')}
            />
            <Text style={styles.datas}>
                Jhon Doe User 
            </Text>
            </View>
            <View style={styles.whiteBlockData}>
            <Image
                style= {styles.icon}
                source= {require('../assets/png/Phone_Icon.png')}
            />
            <Text style={styles.datas}>
                (312) 332-1123
            </Text>
            </View>
            <View style={styles.whiteBlockData}>
            <Image
                style= {styles.icon}
                source= {require('../assets/png/Calendar_Icon.png')}
            />
            <Text style={styles.datas}>
                10-07-2000 
            </Text>
            </View>
            <View style={styles.whiteBlockData}>
            <Image
                style= {styles.icon}
                source= {require('../assets/png/Mail_Icon.png')}
            />
            <Text style={styles.datas}>
                JhonDoe@gmail.com
            </Text>
            </View>
        </View>
        <View style={styles.menuFoodie}>
            
            <Image
            style= {styles.iconMenu}
            source= {require('../assets/svg/Home.svg')}
            />
            <Image
            style= {styles.iconMenu}
            source= {require('../assets/svg/Knife.svg')}
            />
            <Image
            style= {styles.iconMenu}
            source= {require('../assets/svg/Plus.svg')}
            />
            <Image
            style= {styles.iconMenu}
            source= {require('../assets/svg/Store.svg')}
            />
            <Image
            style= {styles.iconMenu}
            source= {require('../assets/svg/User.svg')}
            />
        </View>
        </View>
    );
}

const styles = StyleSheet.create({
    profileContainer:{
        height: 286,
    },
    redBlock:{
        backgroundColor: '#EB5757',
        height: 245,
    },
    profile:{
        textAlign: "center",
        position: "absolute",
        alignSelf: "center",
        marginTop: 60,
        //fontFamily: "inner",
        fontStyle: "normal",
        fontSize: 30,
        lineHeight: 30,
        fontWeight: "600",
        width: 94,
        height: 36,
        color: '#FFFFFF',
        
    },
    logOut:{
        textAlign: "center",
        position: "absolute",
        alignSelf: "flex-end",
        marginTop: 60,
        marginRight: 20,
        //fontFamily: "inner",
        fontStyle: "normal",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "500",
        width: 64,
        height: 19,
        color: '#FFFFFF',
    },
    imageUser:{
        width:160,
        height:160,
        alignSelf:"center",
        marginTop: 127,
        position: "absolute", 
    },
    whiteBlockName:{
        backgroundColor: '#FFFFFF',
        height: 50,
        marginBottom: 24,
    },
    userName:{
        textAlign: "center",
        position: "absolute",
        alignSelf: "center",
        marginTop: 15,
        //fontFamily: "inner",
        fontStyle: "normal",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    whiteBlockData:{
        backgroundColor: '#FFFFFF',
        height: 64,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        borderBottomWidth: 2,
        borderColor: "#E8E8E8",
    },
    datas:{
        textAlign: "center",
        alignSelf: "center",
        position: "absolute",
        margin: 16,
        marginLeft: 96,
        //fontFamily: "inner",
        fontStyle: "normal",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "500",
        height: 19,
        color: '#000000',
    },
    icon:{
        width: 25,
        height: 25,
        marginLeft: 36,
        position: "absolute",
    },
    menuFoodie:{
        marginTop: 110,
        width: 375,
        height: 83,
        backgroundColor: "#FAFAFA",
        flexWrap:"wrap",
        flexDirection: "row",
        borderTopWidth:1,
        justifyContent: "space-around",
        borderColor: "#E8E8E8",

    },
    iconMenu:{
        width: 30,
        height: 30,  
        marginTop: 18,
    },
});

export default Profile;