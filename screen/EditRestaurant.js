import React from "react";
import { StyleSheet, Text, TextInput, View, Pressable, Image, Button, TouchableHighlight } from "react-native";

const onPressClose = () => {
  console.log("CLOSE PRESSED")
}
const onPressUpdate = () => {
  console.log("UPDATE PRESSED")
}

const SettingRestaurant = () => {
  return (
    <View style={[styles.container, {
      flexDirection: "column", 
      top: 70,
    }]}>
        <View style={{flexDirection: "row"}}>
        <Pressable onPress={onPressClose} style={{right: 100} }>
            <Text style={{fontSize: 25} }>x</Text>
        </Pressable>
        
        <Text style={[styles.Oops] }>Settings</Text>

        </View>        
        <TextInput
        style={styles.input}
        // onChangeText={}
        // value={}
        placeholder="Restaurant Name"
      />
      <TextInput
        style={styles.input}
        // onChangeText={}
        // value={}
        placeholder="Restaurant Address"
      />
      <TextInput
        style={styles.input}
        // onChangeText={}
        // value={}
        placeholder="Restaurant RFC"
      />
    <TextInput
        style={styles.input}
        // onChangeText={}
        // value={}
        placeholder="Restaurant type of food"
      />



    <View  style ={{ paddingTop: 16,}}>            
        <TouchableHighlight onPress={onPressUpdate} style={[styles.btn]}>        
        <Text style={[styles.btnText] }>Update</Text>
        </TouchableHighlight>
    </View> 
 
</View>
    
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    fontFamily: "Inter",
  },
  input: {
    width: 343,
    height: 50,
    borderRadius: 8,
    backgroundColor: "#F6F6F6",
    top: 32,
    padding: 16,
    margin:16,
    color: "black"
  },
  Oops: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: "bold",
  },
  btn: {
    overflow: "hidden",
    borderRadius: 100,
    backgroundColor: "#EB5757",
    height: 51,
    width: 343,
    top:200,
  },
  btnText: {
    color: "white",
    textAlign: "center",
    fontSize: 16,
    paddingTop: 16,
  }
});

export default SettingRestaurant;