import React from "react";
import { StyleSheet, Text, View, Pressable, Image, Button, TouchableHighlight } from "react-native";

const onPressSignUp = () => {
  console.log("SIGN UP PRESSED")
}
const onPressLogIn = () => {
  console.log("LOG IN PRESSED")
}

const NoProfile = () => {
  return (
    <View style={[styles.container, {
      flexDirection: "column"
    }]}>
        <View style={[styles.containerRed] } >          
            <Pressable onPress={onPressSignUp} >
                  <Text style={[styles.containerRedText] }>Sign Up</Text>
            </Pressable>
            <Text style={[styles.containerRedTitle]}>Profile</Text>
            <Pressable onPress={onPressLogIn} >
                  <Text style={[styles.containerRedText] }>Log In</Text>
            </Pressable>
        </View>
        <Image source={require('../img/Icons/User.png')} style={[styles.tinyLogo]} />
        <View style={[styles.containerWhite] } >
            <Text style={[styles.Oops] }>Oops!</Text>
            <Text style={[styles.message] }>Please Log in to view your profile {"\n"} information, register a restaurant or a {"\n"} meal.</Text>
            <View  style ={{ paddingTop: 16,}}>
            
              <TouchableHighlight onPress={onPressLogIn} style={[styles.btn]}>        
                <Text style={[styles.btnText] }>Log In</Text>
              </TouchableHighlight>
            </View> 
            <Pressable onPress={onPressSignUp} >
                  <Text style={[styles.lastPressable] }>Don’t have and account? Sign Up!</Text>
            </Pressable>
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    textAlign: "center",
    fontFamily: "Inter",
  },
  containerRed: {
    height: 245,
    backgroundColor: "#EB5757",
    flexDirection: "row",
    justifyContent: "center",
  },
  containerRedText: {
    color: "#ffffff",
    marginTop: 68,
    fontSize: 16,
  },
  containerRedTitle: {
    color: "#ffffff",
    marginTop: 60,
    fontSize: 30,
    paddingLeft: 70,
    paddingRight: 70,    
  },
  tinyLogo: {
    width: 182,
    height: 182,
    marginTop: 127.37,
    alignSelf: "center",
    position: "absolute",    
  },
  containerWhite: {
    position: "absolute",
    paddingTop: 314,
    alignSelf: "center",
  },
  Oops: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: "bold",
  },
  message: {
    fontSize: 16,
    textAlign: "center",
    color: "#666666",
    paddingTop: 16,
  },
  lastPressable: {
    paddingTop: 16,
    fontSize: 16,
    textAlign: "center",
    color: "#EB5757",
    fontWeight: "bold",
  },
  btn: {
    overflow: "hidden",
    borderRadius: 100,
    backgroundColor: "#EB5757",
    height: 51,
  },
  btnText: {
    color: "white",
    textAlign: "center",
    fontSize: 16,
    paddingTop: 16,
  }
});

export default NoProfile;